package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"sort"
	"strings"
)

var gameDb map[int]*Game

func main() {
	bggFile := flag.String("bggFile", "BGStatsExport.json", "File exported from BGG to be processed for Rawgraphs. Defaults to \"BGStatsExport.json\"")
	csvFile := flag.String("csvFile", "BGRawGraphs.csv", "File output from this process. Defaults to \"BGRawGraphs.csv\"")
	flag.Parse()

	export, err := ioutil.ReadFile(*bggFile)
	if err != nil {
		panic(err)
	}

	bgstats := &BGStats{}

	if err := json.Unmarshal(export, &bgstats); err != nil {
		panic(err)
	}

	gameDb = make(map[int]*Game)

	bgstats.StripTimeFromPlayDates()

	// map[<type>]struct{} is idiomatic go for a set of type <type>
	playDates := make(map[string]struct{})

	for _, play := range bgstats.Plays {
		game := getGameByID(play.GameRefID, bgstats)
		game.TotalPlayed++
		game.DatesPlayed[play.PlayDate]++
		playDates[play.PlayDate] = struct{}{}
	}

	sortedDates := sortPlayDates(playDates)
	csvLines := []string{"Game,Date,PlayedOnThisDate,PlayedRunningTotal,PlayedTotal"}
	for _, game := range gameDb {
		playedSoFar := 0
		for _, date := range sortedDates {
			playedOnDate := game.DatesPlayed[date]
			playedSoFar = playedSoFar + playedOnDate
			csvLines = append(csvLines, fmt.Sprintf("\"%s\",%s,%d,%d,%d", game.Name, date, playedOnDate, playedSoFar, game.TotalPlayed))
		}
	}

	ioutil.WriteFile(*csvFile, []byte(strings.Join(csvLines, "\n")), 0644)
}

func sortPlayDates(playDates map[string]struct{}) []string {
	sortedDates := make([]string, 0)

	for date := range playDates {
		sortedDates = append(sortedDates, date)
	}

	sort.Strings(sortedDates)
	return sortedDates
}

func getGameByID(id int, bgstats *BGStats) *Game {
	game := gameDb[id]
	if game != nil {
		return game
	}

	for _, bgGame := range bgstats.Games {
		if bgGame.ID == id {
			datesPlayed := make(map[string]int)
			gameDb[id] = &Game{ID: bgGame.ID, Name: bgGame.Name, DatesPlayed: datesPlayed}
			return gameDb[id]
		}
	}

	return nil
}
