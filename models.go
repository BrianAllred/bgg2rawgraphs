package main

// BGStats is the struct respresenting the json exported from BGG
type BGStats struct {
	Challenges []interface{} `json:"challenges"`
	UserInfo   struct {
		MeRefID     int    `json:"meRefId"`
		BggUsername string `json:"bggUsername"`
	} `json:"userInfo"`
	Games []struct {
		ID               int    `json:"id"`
		UUID             string `json:"uuid"`
		NoPoints         bool   `json:"noPoints"`
		HighestWins      bool   `json:"highestWins"`
		Cooperative      bool   `json:"cooperative"`
		URLThumb         string `json:"urlThumb"`
		ModificationDate string `json:"modificationDate"`
		UsesTeams        bool   `json:"usesTeams"`
		BggID            int    `json:"bggId"`
		BggName          string `json:"bggName"`
		BggYear          int    `json:"bggYear"`
		Name             string `json:"name"`
		Designers        string `json:"designers"`
	} `json:"games"`
	Plays []struct {
		UUID           string `json:"uuid"`
		Ignored        bool   `json:"ignored"`
		Rating         int    `json:"rating"`
		ScoringSetting int    `json:"scoringSetting"`
		PlayerScores   []struct {
			Winner      bool   `json:"winner"`
			SeatOrder   int    `json:"seatOrder"`
			Score       string `json:"score"`
			StartPlayer bool   `json:"startPlayer"`
			PlayerRefID int    `json:"playerRefId"`
			Rank        int    `json:"rank"`
			NewPlayer   bool   `json:"newPlayer"`
			Team        string `json:"team,omitempty"`
		} `json:"playerScores"`
		ModificationDate string `json:"modificationDate"`
		BggLastSync      string `json:"bggLastSync,omitempty"`
		PlayDate         string `json:"playDate"`
		ManualWinner     bool   `json:"manualWinner"`
		LocationRefID    int    `json:"locationRefId,omitempty"`
		Rounds           int    `json:"rounds"`
		UsesTeams        bool   `json:"usesTeams"`
		BggID            int    `json:"bggId"`
		EntryDate        string `json:"entryDate"`
		PlayImages       string `json:"playImages"`
		DurationMin      int    `json:"durationMin"`
		NemestatsID      int    `json:"nemestatsId"`
		GameRefID        int    `json:"gameRefId"`
		Comments         string `json:"comments,omitempty"`
	} `json:"plays"`
	Locations []struct {
		ID               int    `json:"id"`
		ModificationDate string `json:"modificationDate"`
		Name             string `json:"name"`
		UUID             string `json:"uuid"`
	} `json:"locations"`
	Players []struct {
		ID               int    `json:"id"`
		IsAnonymous      bool   `json:"isAnonymous"`
		ModificationDate string `json:"modificationDate"`
		Name             string `json:"name"`
		UUID             string `json:"uuid"`
		BggUsername      string `json:"bggUsername,omitempty"`
	} `json:"players"`
}

// Game is a struct representing a specific board game
type Game struct {
	ID          int
	Name        string
	DatesPlayed map[string]int
	TotalPlayed int
}

// StripTimeFromPlayDates sets each Play struct's PlayDate to just the date value
func (bgstats *BGStats) StripTimeFromPlayDates() {
	for index, play := range bgstats.Plays {
		play.PlayDate = play.PlayDate[:10]
		bgstats.Plays[index] = play
	}
}
